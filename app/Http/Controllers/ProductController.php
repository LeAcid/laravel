<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;

class ProductController extends Controller
{
    public $products = [];

    public function index() 
    {
        $items = [];
        if (Storage::exists('products.json')) {
            $items = json_decode(Storage::get('products.json'));
        }
        return response()->json([
            "items" => $items
        ]);
    }

    public function create(Request $request)
    {
        try {
            if (Storage::exists('products.json')) {
                $this->products = json_decode(Storage::get('products.json'));
            }

            $product = [
                "product_name"      =>  $request->product_name,
                "quantity_in_stock" =>  $request->quantity_in_stock,
                "price_per_item"    =>  (float) $request->price_per_item,
                "date_submitted"    => Carbon::now()->toDateTimeString(),
                "total_value"       => $request->quantity_in_stock * $request->price_per_item
            ];

            array_push($this->products, $product);

            Storage::put('products.json', json_encode($this->products));
        } catch(\Exception $e) {
            dd($e);
            return response()->json([
                "error"     => true,
                "message"   => $e->getMessage()
            ], 400);
        }
        return response()->json([
            "error"     => false,
            "message"   => "Product saved!"
        ], 201);
    }
}
